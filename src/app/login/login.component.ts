import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  userForm: any;

  constructor(private formBuilder: FormBuilder, private router: Router) { }

  ngOnInit() {
    this.userForm = this.formBuilder.group({
      username: ['', Validators.required],
      password: ['', [Validators.required]]
    });
    this.checkLogin();
  }

  LoginUser() {
    let value = this.userForm.value
    if (value.username && value.password) {
      let loginDetail = {
        "username": value.username,
        "password": value.password
      }
      localStorage.setItem("login", JSON.stringify(loginDetail));
      this.router.navigate(['/images'])
    }
  }

  checkLogin() {
    let login = localStorage.getItem("login");
    let loginDetail = JSON.parse(login);
    if (loginDetail && loginDetail['username'] && loginDetail['password']) {
      this.router.navigate(['/images'])
    } else {
      this.router.navigate(['/login'])
    }
  }

}
