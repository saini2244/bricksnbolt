import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ActivatedRoute, Router } from '@angular/router';


@Component({
  selector: 'app-image-layout',
  templateUrl: './image-layout.component.html',
  styleUrls: ['./image-layout.component.scss']
})
export class ImageLayoutComponent implements OnInit {

  private url = "https://www.flickr.com/services/rest/?method=flickr.groups.pools.getPhotos&api_key=667a6ce5f358b237ec13e9d99bb659de&group_id=2309748%40N20&format=json&nojsoncallback=1";
  private photos: any = []

  constructor(private http: HttpClient, private activateRoute: ActivatedRoute,
    private router: Router) { }

  ngOnInit() {
    this.getImageData();
  }

  getImageData() {
    this.http.get(this.url).subscribe(data => {
      if (data['photos']) {
        this.photos = data['photos']['photo']
      }
    }, error => {
      console.log(error)
    })
  }

}
