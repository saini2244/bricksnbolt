import { Component } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'bricksnbolt';

  constructor(private router: Router) {

  }

  ngOnInit() {
    let login = localStorage.getItem("login");
    let loginDetail = JSON.parse(login);
    if (loginDetail && loginDetail['username'] && loginDetail['password']) {
      this.router.navigate(['/images'])
    } else {
      this.router.navigate(['/login'])
    }
  }

}
